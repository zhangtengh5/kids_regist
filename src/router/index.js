import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store';
import { getQueryString } from '@/assets/js/handleUrl.js'
import Telephone from '@/components/telephone'
import Success from '@/components/success'
import Failed from '@/components/failed'
import Plam from '@/components/plam'
import Login from '@/components/login'
import LoginFail from '@/components/login_fail'
import LoginSuc from '@/components/login_success'
import Account from '@/components/account'
import Select from '@/components/select'
Vue.use(Router)
let router = new Router({
  routes: [
    {
      path: '/regist',
      name: 'regist',
      component: Telephone
    },
    {
      path: '/success',
      name: 'success',
      component: Success
    },
    {
      path: '/failed',
      name: 'failed',
      component: Failed
    },
    {
      path: '/plam',
      name: 'plam',
      component: Plam
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/select',
      name: 'select',
      component: Select,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/fail',
      name: 'fail',
      component: LoginFail
    },
    {
      path: '/suc',
      name: 'suc',
      component: LoginSuc
    },
    {
      path: '/account',
      name: 'account',
      component: Account,
      meta: {
        requiresAuth: true
      }
    }
  ]
})
router.beforeEach((to, from, next) => {
  store.state.equipmentId = getQueryString('id');
  if (from.path === '/regist' || '/plam' || '/failed' || '/success' ||
    '/suc' || '/fail' || '/login' || '/account' || '/select') {
    clearInterval(store.state.timer);
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    let accountId = store.state.accountId;
    if (accountId) {
      next()
    } else {
      next({
        path: '/'
      })
    }
  } else {
    next() // 确保一定要调用 next()
  }
})
export default router
