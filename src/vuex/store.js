/**
 * plamStep手掌注册处于的阶段
 *  0: 默认
 *  1: 开启手掌验证
 *  2: 验证手掌数据获取完成
 *  3: 验证完成开始注册
 *  4: 采集手掌数据获取完成
 */
import Vuex from 'vuex'
import axios from '../axios/http';
import router from '../router';
import {Message} from 'element-ui';
import Vue from 'vue'
Vue.use(Vuex);
let store = new Vuex.Store({
    state: {
      isAdmin: false,
      isLogin: true,
      equipmentId: null,
      telephoneInfo: null,
      timer: null,
      activexObj: null,
      statusInfo: null,
      message_regist: '请返回首页重新尝试',
      message_login: '未识别到您的掌静脉信息',
      softProgress: null,
      LoginProgress: null,
      againRegist: false,
      plamStep: 0,
      showLoad: false,
      title: '加载中...',
      codeData: [],
      accountId: null,
      reTime: 0,
      oldreTime: 3
    },
    getters: {
      
    },
    mutations: {
      startRegist (state, payload) {
        //注册开启手掌验证
        state.isLogin = false;
        router.push('/plam');
        state.telephoneInfo = payload.telephoneInfo;
        state.activexObj.capturePalmData();
        state.plamStep = 1;
      },
      startLogin (state, payload) {
        state.isLogin = true;
        router.push('/login');
        state.activexObj.capturePalmData();
        state.plamStep = 1;
      },
      capturePalmData (state, payload) {
        //验证手掌数据获取完成
        state.plamStep = 2; 
        let veinData = payload.verifyData;
        axios.post('/vein/find.do', {
          veinData: veinData,
          equipmentName: state.equipmentId,
          noload: true
        }).then((response) => {
          let result_data = response.data;
          if(parseInt(result_data.result) === 0) {
            //验证完成掌静脉已存在
            if (state.isLogin) {
              //登录成功
              state.accountId = result_data.id;
              if (state.accountId) {
                if (state.isAdmin) {
                  //验证管理员权限
                  axios.post('/middle/checkPerson.do', {
                    veinId: state.accountId,
                    snCode: state.equipmentId
                  })
                  .then((response) => {
                    let check_data = response.data;
                    if (parseInt(check_data.status) === 1) {
                      router.push('/account');
                    } else {
                      state.message_login = check_data.msg;
                      router.push('/fail');
                    }
                  })
                  .catch((error) => {
                    Message({
                      message: error.message,
                      type: 'error',
                      duration: 1500
                    });
                  })
                } else {
                  //验证家长权限
                  axios.post('/zd/Check.do', {
                    userId: state.accountId
                  })
                  .then((response) => {
                    let check_data = response.data;
                    if (parseInt(check_data.status) === 1) {
                      router.push('/select');
                    } else {
                      state.message_login = check_data.msg;
                      router.push('/fail');
                    }
                  })
                  .catch((error) => {
                    Message({
                      message: error.message,
                      type: 'error',
                      duration: 1500
                    });
                  })
                 
                }
              } else {
                state.message_login = '您的掌静脉信息不存在';
                router.push('/fail');
              }
              state.plamStep = 0;
              state.reTime = 0;
            } else {
              //注册失败
              state.message_regist = '您的掌静脉信息已经被注册';
              router.push('/failed');
              window.external.playMusic(6);
              state.plamStep = 0;
            }
          } else {
            //验证完成掌静脉不存在
            if (state.isLogin) {
              //登录失败
              state.reTime++;
              if (state.reTime > state.oldreTime) {
                state.reTime = 0;
                state.plamStep = 0;
                state.message_login = '未识别到您的掌静脉信息';
                router.push('/fail');
              } else {
                state.activexObj.capturePalmData(); //又开始登录
                state.plamStep = 1;
              }
            } else {
              //开始注册
              state.activexObj.enrollBtnClick();
              state.plamStep = 3;
            }
          }
        }).catch((error)=> {
          Message({
            message: error.message,
            type: 'error',
            duration: 1500
          });
        })
      },
      enrollData (state, payload) {
        //采集手掌数据获取完成
        state.plamStep = 4; 
        let registData = payload.registData;
        let telephoneInfo = state.telephoneInfo;
        axios.post('/middle/InsertVein.do', {
          phone: telephoneInfo.phone_num,
          vein: registData
        }).then((response) => {
          let result_data = response.data;
          if(parseInt(result_data.status) === 1) {
            //注册成功
            router.push('/success');
            window.external.playMusic(5);
          } else {
             //注册失败
            state.message_regist = result_data.msg;
            router.push('/failed');
          }
          state.plamStep = 0;
        }).catch((error) => {
          Message({
            message: error.message,
            type: 'error',
            duration: 1500
          });
        })
      },
      registStatus (state, payload) {
        //注册过程的状态信息
        let statusInfo = payload.statusInfo;
        let softProgress = state.softProgress;
        let LoginProgress = state.LoginProgress;
        if (state.isLogin) {
          LoginProgress(statusInfo);
        } else {
          softProgress(statusInfo);
        }
      },
      againRegistPalm (state, payload) {
        //重新注册
        if (state.plamStep == 1) {
          state.againRegist = true;
          state.activexObj.cancleCapturePalm();//取消验证
        } else if (state.plamStep == 3) {
          state.againRegist = true;
          state.activexObj.cancleEnroll();//取消注册
        } else {
          state.activexObj.capturePalmData(); //开启验证
          state.plamStep = 1;
        }
      },
      cancleSoft (state, payload) {
        //取消注册
        if (state.plamStep == 1) {
          state.activexObj.cancleCapturePalm();//取消验证
        } else if (state.plamStep == 3) {
          state.activexObj.cancleEnroll();//取消注册
        }
        state.plamStep = 0;
      },
      cancleAllSoft (state, payload) {
        //返回
        state.activexObj.cancleEnroll();//取消注册
        state.activexObj.cancleCapturePalm();//取消验证
      },
      finishSoft (state, payload) {
        state.activexObj.Ps_Sample_Apl_CS_FinishLibrary();//关闭掌静脉
      }
    }
})

export default store