import axios from 'axios'
import qs from 'qs';
import store from '@/vuex/store';
axios.defaults.timeout = 20000;

axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? '/' : '/api';
// http request 拦截器
axios.interceptors.request.use(
  config => {
    
    if (config.method == "post") {
      if (config.data.noload === true) {
        store.state.showLoad = false;
        delete config.data.noload;
      } else {
        store.state.showLoad = true;
      }
      config.data = qs.stringify(config.data);
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }
    return config;
  },
  err => {
    store.state.showLoad = false;
    return Promise.reject(err);
  });

// http response 拦截器
axios.interceptors.response.use(
  response => {
    store.state.showLoad = false;
    return response;
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = '请求错误(400)';
          break;
        case 401:
          error.message = '(401)';
          break;
        case 403:
          error.message = '拒绝访问(403)';
          break;
        case 404:
          error.message = '请求出错(404)';
          break;
        case 408:
          error.message = '请求超时(408)';
          break;
        case 500:
          error.message = '服务器错误(500)';
          break;
        case 501:
          error.message = '服务未实现(501)';
          break;
        case 502:
          error.message = '网络错误(502)';
          break;
        case 503:
          error.message = '服务不可用(503)';
          break;
        case 504:
          error.message = '网络超时(504)';
          break;
        case 505:
          error.message = 'HTTP版本不受支持(505)';
          break;
        default:
          error.message = `连接出错(${error.response.status})!`;
      }
    } else {
      error.message = '网络不稳定,请稍后重试'
    }
    store.state.showLoad = false;
    return Promise.reject(error);
  });

export default axios;
